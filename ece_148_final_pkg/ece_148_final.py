from gc import callbacks
import rclpy
from rclpy.node import Node

from rclpy.qos import ReliabilityPolicy, QoSProfile

import sys
import os
os.environ['OPENBLAS_NUM_THREADS'] = str(1)
import numpy as np
import datetime
import json
import time
import configparser
import graph_ltpl
from nav_msgs.msg import Odometry

from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener


from tf_transformations import euler_from_quaternion


from nav_msgs.msg import Path
from geometry_msgs.msg import PoseStamped

class ECE_148_final(Node):
    def __init__(self):
       
        super().__init__('ece_148_final')
        
        timer_period = 0.5
        self.timer1 = self.create_timer(timer_period, self.sending_path)
        self.traj_set = {'straight': None}
        #self.obj_list_dummy = graph_ltpl.testing_tools.src.objectlist_dummy.ObjectlistDummy(dynamic=False,vel_scale=0.3,s0=250.0)
        self.path_pub = self.create_publisher(Path, 'path_publisher', 10)
        self.perc_sub = self.create_subscription(Odometry, '/ego_racecar/odom', self.perception_callback, QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT))
        self.first_pose = False
        self.tf_buffer = Buffer()
        self.tf_listener = TransformListener(self.tf_buffer, self)
                               
        toppath = "/home/projects/ros2_ws/src/ece_148_final/ece_148_final_pkg" #this is a harcoded path, change to you own                                                      

        track_param = configparser.ConfigParser()
        if not track_param.read("/home/projects/ros2_ws/src/ece_148_final/ece_148_final_pkg/params/driving_task.ini"): #this is a harcoded path, change to you own 
            raise ValueError('Specified online parameter config file does not exist or is empty!')

        track_specifier = json.loads(track_param.get('DRIVING_TASK', 'track'))

        # define all relevant paths
        path_dict = {'globtraj_input_path': toppath + "/inputs/traj_ltpl_cl/traj_ltpl_cl_" + track_specifier + ".csv",
                    'graph_store_path': toppath + "/inputs/stored_graph.pckl",
                    'ltpl_offline_param_path': toppath + "/params/ltpl_config_offline.ini",
                    'ltpl_online_param_path': toppath + "/params/ltpl_config_online.ini",
                    'log_path': toppath + "/logs/graph_ltpl/",
                    'graph_log_id': datetime.datetime.now().strftime("%Y_%m_%d__%H_%M_%S")
                    }

        # ----------------------------------------------------------------------------------------------------------------------
        # INITIALIZATION AND OFFLINE PART --------------------------------------------------------------------------------------
        # ----------------------------------------------------------------------------------------------------------------------

        # intialize graph_ltpl-class
        self.ltpl_obj = graph_ltpl.Graph_LTPL.Graph_LTPL(path_dict=path_dict,
                                                    visual_mode=False,
                                                    log_to_file=False)

        # calculate offline graph
        self.ltpl_obj.graph_init()

        
        
        self.path = Path()

    def sending_path(self):

        if not self.first_pose:
            return
        
        for self.sel_action in ["right", "left", "straight", "follow"]:  # try to force 'right', else try next in list
            if self.sel_action in self.traj_set.keys():
                break

        #self.obj_list = self.obj_list_dummy.get_objectlist()

        self.ltpl_obj.calc_paths(prev_action_id=self.sel_action,object_list=[])

        self.traj_set = self.ltpl_obj.calc_vel_profile(pos_est=self.pos_est,vel_est=self.vel_est)[0]
    
        self.path.header.frame_id = 'map'

        #self.get_logger().info("sel_action: generic3")
        
        #if self.sel_action in self.traj_set:
        for row in self.traj_set[self.sel_action][0]:
            pose_msg = PoseStamped()
            pose_msg.pose.position.x = row[1]
            pose_msg.pose.position.y = row[2]

            self.path.poses.append(pose_msg)
        self.path_pub.publish(self.path)
        self.path = Path()
        #self.get_logger().info("pose_msg: {%s}" % (pose_msg))
        #self.get_logger().info("stuff: {%s}" % (pose_msg.pose.position.x))
        #self.get_logger().info("stuff: {%s}" % (self.path))
        #self.get_logger().info("sel_action: {%s}" % (self.sel_action))
        #self.get_logger().info("traj_set: {%s}" % (self.traj_set.keys()))
            



    def perception_callback(self, odom_data):
        self.x_buffer = odom_data.pose.pose.position.x
        self.y_buffer = odom_data.pose.pose.position.y
        self.vx_buffer = odom_data.twist.twist.linear.x
        self.vy_buffer = odom_data.twist.twist.linear.y
        self.get_logger().info(f"self.pos_est: {[self.x_buffer, self.y_buffer]}")
        self.pos_est = tuple([self.x_buffer, self.y_buffer])
        # set start pos
        if not self.first_pose:
            heading_est = euler_from_quaternion((odom_data.pose.pose.orientation.x, odom_data.pose.pose.orientation.y, odom_data.pose.pose.orientation.z, odom_data.pose.pose.orientation.w))[2]
            self.ltpl_obj.set_startpos(pos_est=self.pos_est,
                                heading_est=heading_est)
            self.first_pose = True
        
        self.get_logger().info(f"self.pos_est: {[self.x_buffer, self.y_buffer]}")
        # self.pos_est = {[self.x_buffer, self.y_buffer]}
        self.vel_est = self.vx_buffer
            
            
def main(args=None):
    # initialize the ROS communication
    rclpy.init(args=args)
    # declare the node constructor
    ece_148_final = ECE_148_final()       
    # pause the program execution, waits for a request to kill the node (ctrl+c)
    rclpy.spin(ece_148_final)
    # Explicity destroy the node
    ece_148_final.destroy_node()
    # shutdown the ROS communication
    rclpy.shutdown()

if __name__ == '__main__':
    main()
